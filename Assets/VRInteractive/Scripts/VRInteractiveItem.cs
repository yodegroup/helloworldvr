using System;
using UnityEngine;

namespace VRInteractive
{
    // This class should be added to any gameobject in the scene
    // that should react to input based on the user's gaze.
    // It contains events that can be subscribed to by classes that
    // need to know about input specifics to this gameobject.
    public class VRInteractiveItem : MonoBehaviour
    {
        // Called when the gaze moves over this object
        public event Action OnOver;
        // Called when the gaze leaves this object            
        public event Action OnOut;
        // Called when click input is detected whilst the gaze is over this object.            
        public event Action OnClick;
        // Called when Fire1 is released whilst the gaze is over this object.          
        public event Action OnUp;
         // Called when Fire1 is pressed whilst the gaze is over this object.               
        public event Action OnDown;            


		protected bool m_IsOver;
			
		public bool IsOver
        {
            get { return m_IsOver; }              // Is the gaze currently over this object?
        }


        // The below functions are called by the VREyeRaycaster when the appropriate input is detected.
        // They in turn call the appropriate events should they have subscribers.
		public virtual void Over()
        {			
            m_IsOver = true;

            if (OnOver != null)
                OnOver();
        }


		public virtual void Out()
        {
            m_IsOver = false;
            if (OnOut != null)
                OnOut();
        }


		public void Click()
        {
			if (!m_IsOver) {
				return;
			}
			if (OnClick != null) {
				OnClick ();
			}
			SelectionCompleted ();
        }

		public virtual void SelectionCompleted()
		{
			Debug.Log("SelectionCompleted");
			if (OnClick != null) {
				OnClick ();
			}
		}

		public void Up()
        {
			if (!m_IsOver) {
				return;
			}

			if (OnUp != null) {
				OnUp ();
			}
        }

	
		public virtual void Down()
        {
			if (OnDown != null) {
				OnDown ();
			}
        }
    }
}