﻿using UnityEngine;
using System.Collections;
using VRInteractive;
using System;


namespace VRInteractive
{
	/// <summary>
	/// Объект, который можно выбрать взглядом. 
	/// </summary>
	[RequireComponent(typeof(VRInteractiveItem))]
	public class GazeSelectableItem : MonoBehaviour {
		
		/// <summary>
		/// "Круговой" индикатор прогресса, используемый для отображения состояния выбора
		/// </summary>
		private SelectionRadial _selectionRadial;

		private VRInteractiveItem _vrInteractiveItem;
		/// <summary>
		/// Событие, возникающее когда пользователь выбрал объект
		/// </summary>
		public event Action OnObjectSelected;
		

		// Use this for initialization
		void Awake () {		
			_selectionRadial = FindObjectOfType<SelectionRadial> ();
		}		

		void OnEnable()
		{				
			_selectionRadial.OnSelectionComplete += SelectionRadial_OnSelectionComplete;

			_vrInteractiveItem = GetComponent<VRInteractiveItem> ();
			_vrInteractiveItem.OnOver += VrInteractItem_OnOver;
			_vrInteractiveItem.OnOut += VrInteractItem_OnOut;
		}

		void OnDisable()
		{
			if (_selectionRadial != null) {
				VrInteractItem_OnOut ();//принудительно вызываем метод, для того чтобы "сбросить" выделение с неакивного объекта
				_selectionRadial.OnSelectionComplete -= SelectionRadial_OnSelectionComplete;
			}

			var vrInteractItem = GetComponent<VRInteractiveItem> ();
			vrInteractItem.OnOver -= VrInteractItem_OnOver;
			vrInteractItem.OnOut -= VrInteractItem_OnOut;
		}

		void SelectionRadial_OnSelectionComplete ()
		{		
			if (_vrInteractiveItem.IsOver && OnObjectSelected != null) {			
				OnObjectSelected ();
			}
		}		

		void VrInteractItem_OnOut ()
		{
			_selectionRadial.Hide ();
		}

		void VrInteractItem_OnOver ()
		{
			_selectionRadial.Show ();		
		}				
	}
}