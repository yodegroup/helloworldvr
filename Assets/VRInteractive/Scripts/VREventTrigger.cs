﻿using UnityEngine;
using UnityEngine.Events;
using VRInteractive;
using System;
using System.Collections;
using System.Collections.Generic;
 
[Serializable]
public class GazeEvent : UnityEvent {}
 
[RequireComponent (typeof(GazeSelectableItem))]
public class VREventTrigger : MonoBehaviour 
{ 
	public GazeEvent OnGazeSelectionComplete;
	public GazeEvent OnOver;
	public GazeEvent OnOut;

	void OnEnable()
	{
		var _VRInteractiveItem = GetComponent<VRInteractiveItem> ();
		_VRInteractiveItem.OnOver += Over;
		_VRInteractiveItem.OnOut += Out;
		var _gazeSelectableItem = GetComponent<GazeSelectableItem>();
		_gazeSelectableItem.OnObjectSelected += SelectionCompleted;
	}

	void OnDisable()
	{
		var _VRInteractiveItem = GetComponent<VRInteractiveItem> ();
		_VRInteractiveItem.OnOver -= Over;
		_VRInteractiveItem.OnOut -= Out;
		_VRInteractiveItem.OnClick -= SelectionCompleted;
	}
		
	void Over ()
	{
		OnOver.Invoke ();
	}

	void Out ()
	{
		OnOut.Invoke ();
	}

	void SelectionCompleted ()
	{
		print ("SelectionCompleted "+gameObject.name);
		OnGazeSelectionComplete.Invoke ();
	}
 }