using UnityEngine;
using UnityEngine.UI;

namespace VRInteractive
{
    // The reticle is a small point at the centre of the screen.
    // It is used as a visual aid for aiming. The position of the
    // reticle is either at a default position in space or on the
    // surface of a VRInteractiveItem as determined by the VREyeRaycaster.
    public class Reticle : MonoBehaviour
    {
        [SerializeField] private Image m_Image;                     // Reference to the image component that represents the reticle.
        [SerializeField] private Transform m_ReticleTransform;      // We need to affect the reticle's transform.
        private Vector3 m_OriginalScale;                            // Since the scale of the reticle changes, the original scale needs to be stored.
        private Quaternion m_OriginalRotation;                      // Used to store the original rotation of the reticle.
		private Vector2 m_OriginalSize;  

        private void Awake()
        {
			m_OriginalSize = m_ReticleTransform.GetComponent<RectTransform> ().sizeDelta;
        }
			
        public void Hide()
        {
//            m_Image.enabled = false;
        }
			
        public void Show()
        {
//            m_Image.enabled = true;
        }
			
        public void SetPosition (RaycastHit hit)
        {
//            m_ReticleTransform.position = hit.point;
////            m_ReticleTransform.localScale = m_OriginalScale * hit.distance;
//			m_ReticleTransform.GetComponent<RectTransform>().sizeDelta = m_OriginalSize*hit.distance
//            
//            // If the reticle should use the normal of what has been hit...
//            if (m_UseNormal)
//                // ... set it's rotation based on it's forward vector facing along the normal.
//                m_ReticleTransform.rotation = Quaternion.FromToRotation (Vector3.forward, hit.normal);
//            else
//                // However if it isn't using the normal then it's local rotation should be as it was originally.
//                m_ReticleTransform.localRotation = m_OriginalRotation;
        }
    }
}