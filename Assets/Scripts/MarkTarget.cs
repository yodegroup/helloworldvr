﻿using UnityEngine;

public class MarkTarget : MonoBehaviour
{
    [SerializeField]
    private float _upSpeedMeterPerSecond;

    [SerializeField]
    private float _destroyTimeSecond;

    [SerializeField]
    private ParticleSystem _particleSystem;

    private void Start()
    {
        _particleSystem.GetComponentInChildren<ParticleSystem>();
        Destroy(gameObject, _destroyTimeSecond);
    }

    private void Update()
    {
        transform.Translate(Vector3.up * _upSpeedMeterPerSecond * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Laser")
        {
            _particleSystem.transform.parent = null;
            _particleSystem.Play();
            Destroy(gameObject);
            Destroy(_particleSystem, 1f);
        }
    }
}