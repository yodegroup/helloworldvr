﻿using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField]
    private GameObject _laser;

    private bool _laserActive = false;
    

    void Update()
    {
        if (Input.GetKey(KeyCode.Space) || Input.GetMouseButton(0))
        {
            if (!_laserActive)
            {
                _laserActive = true;
                _laser.SetActive(true);
            }
        }
        else
        {
            if (_laserActive)
            {
                _laserActive = false;
                _laser.SetActive(false);
            }
        }
    }
}